# Week-it <img src = "https://github.com/sungeunDev/EAT_IT/blob/master/Scrennshot/icon.png?raw=true" width = 50 align = right>
<p align="center">
<img src="https://img.shields.io/badge/swift-4.1-blue.svg" />
<img src="https://img.shields.io/badge/xcode-9.3-green.svg" />
<img src="https://img.shields.io/badge/ios-11.0-yellow.svg" />
<img src="https://img.shields.io/badge/contacts-@p.ssungnni-orange.svg" />
<img src="https://img.shields.io/badge/licence-MIT-lightgrey.svg" /> <br><br>
 
 <a href="https://itunes.apple.com/us/app/week-it/id1406693210?mt=8" target="_blank">
 <img src = "https://devimages-cdn.apple.com/app-store/marketing/guidelines/images/badge-download-on-the-app-store.svg"> 
    </a>
     <a href="https://itunes.apple.com/kr/app/week-it/id1406693210?mt=8" target="_blank">
 <img src = "https://devimages-cdn.apple.com/app-store/marketing/guidelines/images/badge-download-on-the-app-store-kr.svg"> 
    </a>
</p>
<br>

> -. 첫번째 개인 앱 프로젝트  
> -. 주간 단위로 일상을 기록하는 어플 Week-it 입니다.


<br>

## Contents
1. [About Week-it](#about-week-it)
2. [Version Update](#version-update)
3. [What I Learned](#what-i-learned)
4. [Things to do next](#things-to-do-next)
5. [Contact](#contact)

<br>

## About Week-it

![ver 1.0](./Scrennshot/ver%201.0.png) 

- Week-it은 2018년 1월에 Swift를 처음 접한 6개월차 초보 개발자의 첫번째 프로젝트입니다.  
- 프로젝트 시작부터 출시까지를 모두 경험하는 것을 목표로 개발뿐만 아니라 기획, 디자인 모두 직접 진행하였습니다.

<br>

>   - Post it! Week it!  
    이번주 당신의 일상은 어떠셨나요?  
    Week-it은 한 주를 의미있게 보냈는지 한 눈에 알 수 있는 주간 기록 어플입니다.  
>    
>   - Rating
    아침, 점심, 저녁 마다 간단하게 일상 생활을 메모하고, 별점을 매겨 보세요.  
    할일을 기록하는 to-do 리스트나 식단 기록장으로 활용해도 좋아요.  
    하나 하나 기록이 쌓여 의미있는 한 주를 만들어 줄 거에요 :)  
>
>    - Analysis   
    나는 어느 요일에, 어느 시간에 가장 중요하고 의미있는 일을 하고 있을까요?  
    통계를 통해 여러분도 몰랐던 생활 패턴을 발견해 보세요.  
    식단을 기록하고 있다면 금요일마다 별 1개짜리 음식을 먹는 당신을 발견할지도 몰라요!  
>
>    -  Colorful Theme  
    다양한 테마를 통해 Week-it에 컬러를 더해보세요.  
    혹시 모든 테마들이 마음에 들지 않는다면, 메일을 통해 테마 색상을 보내주세요!
>
>    - [Youtube Link](https://www.youtube.com/watch?v=l0sixcSdIpw&feature=youtu.be)

<br>

## Version Update
- start: 2018.06.05 ~   
- ver 1.0.0 : US App Store release (18.07.04)
- ver 1.1.0 : KR App Store realease. Localized KR (18.07.08)
- ver 1.2.0 : 주말 on/off 기능 추가. 날짜 형식 변경 가능. 8월 포스트 입력시 앱 종료되는 버그 수정. (18.07.31)

<br>

## What I Learned
- Tech  
    - Realm: 데이터 베이스로 활용  
    - Today Extension: Widget 연동   
    - Date, Calendar 다루기  
    - Frame based UI
    - Custom Delegate 활용


- Etc.  
    - 개발 전 설계에 대한 필요성  
    - 좋은 코드, 잘 만든 코드란?  
  
<br>

## Things to do next 
> -. 앞으로 할일들. 하나씩 지워나가는 게 목표!

- 기능 추가
    + DB Backup - Cloud Kit
    + Custom Animation
    + Push Notification   
    + Search - 포스트 내용 검색

- UI
    + Flexible Post Matrix -> 현재는 아침, 점심 저녁 / 월 ~ 금 고정. 행, 렬 커스텀화
        * 주말 on/off 기능 추가 (7/31)
    + Recent Posts -> 최근 등록한 포스트 보여주기. drag로 복사, 붙여넣기 가능하도록.
    + Change Post Create View -> Present from bottom of MainView


<br>

## Contact
- phone: +82 10.5835.0602
- mail: p.ssungnni@gmail.com
